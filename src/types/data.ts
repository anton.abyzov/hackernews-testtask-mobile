
export type ItemEntity = {
  id: number,
  title: string,
  url: string,
  by: string,
  time: number,
  score: number
};

export type StoryEntity = ItemEntity & {
  author: UserEntity
};

export type UserEntity = {
  id: string,
  karma: number
};
