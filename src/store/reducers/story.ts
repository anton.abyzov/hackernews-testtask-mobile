import { createReducer } from 'redux-act';
import * as actions from '../actions';
import { StoryEntity } from '../../types/data';


export type StoryState = { sampleFromTopStories: Array<StoryEntity> };
const DEFAULT_STATE: StoryState = {
  sampleFromTopStories: []
};

export default createReducer(
  {
    [actions.sampleFromTopStoriesLoaded.getType()]: (state, data: Array<StoryEntity>) => ({
      ...state,
      sampleFromTopStories: data
    }),
  },
  DEFAULT_STATE
);
