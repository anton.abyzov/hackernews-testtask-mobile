import { applyMiddleware, createStore } from 'redux';
import { promiseMiddleware } from './middleware';
import reducer from './reducer';
import { StoryState } from './reducers/story';


export const store = createStore(reducer, applyMiddleware(promiseMiddleware,));

export type RootState = {
  story: StoryState;
};
export type AppDispatch = typeof store.dispatch;
