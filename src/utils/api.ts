import { randomSample } from './array';
import { ItemEntity, UserEntity, StoryEntity } from '../types/data';

const API_ROOT = 'https://hacker-news.firebaseio.com/v0';
const requests = {
  get: (url: string) =>
    fetch(`${API_ROOT}${url}`).then(res => res.json()),
};

type ItemAPI = {
  get: (itemID: (string | number)) => Promise<ItemEntity>
};
const Item: ItemAPI = {
  get: (itemID) => requests.get(`/item/${itemID}.json`)
};


type UserAPI = {
  get: (username: string) => Promise<UserEntity>
};
const User: UserAPI = {
  get: username => requests.get(`/user/${username}.json`)
};



type StoryAPI = {
  getTopStories: () => Promise<number[]>
  getSampleFromTopStories: (number?: number) => Promise<StoryEntity[]>
};
const Story: StoryAPI = {
  getTopStories: () => requests.get('/topstories.json'),
  async getSampleFromTopStories(number = 10) {
    const response = await this.getTopStories();
    const results = await Promise.all(
      randomSample(response, number).map(async newsID => {
        const news = await Item.get(newsID);
        const user = await User.get(news.by);
        return ({ ...news, author: user });
      })
    );

    return results.sort((newsA, newsB) => newsA.score - newsB.score);
  }
};

export default {
  Item,
  User,
  Story
};
